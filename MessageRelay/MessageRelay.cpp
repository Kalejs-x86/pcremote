#include "Networking/NetworkingTCP.hpp"

int main(int argc, char** argv)
{
	try
	{
		std::vector<std::string> Args;
		for (int i = 1; i < argc; ++i)
		{
			Args.emplace_back(argv[i]);
		}
		const size_t ArgCount = Args.size();

		if (ArgCount != 1)
			return 1;

		std::string Action = Args[0];

		Common::Network::TCP::CNetworkTCPClient Client;
		Client.SetRemoteHost("localhost", 8081);
		Client.Connect();
		CHECK(Client.IsConnected());

		NETMESSAGE Msg;
		Msg.SetOperation(Common::Network::TCP::Operation::TestString);
		Msg.FromString(Action);

		CHECK(Client.SendServer(&Msg) == NetResult::Success);
		CHECK(Client.RecvServer(&Msg) == NetResult::Success);
		Client.CloseConnection();
	}
	catch (...)
	{
		return 0xff;
	}
	

	return 0;
}