#pragma once
#include <string>
#include <vector>
#include <Physicalmonitorenumerationapi.h>
#include <Highlevelmonitorconfigurationapi.h>

#include "VolumeControl.hpp"
#include "Shared/Exception.hpp"

#define VK_N 0x4E
#define VK_0 0x30

enum class SkipType : int
{
	// 0 - 9 are for part skips
	Start = 0,
	End = 9,
	Rewind5s,
	Skip5s
};

void SkipTime(HWND Window, int Action)
{
	SkipType Skip = static_cast<SkipType>(Action);
	DWORD VK = 0;
	bool ExtendedKey = false;

	if (Skip >= SkipType::Start && Skip <= SkipType::End)
		VK = VK_0 + Action;
	else if (Skip == SkipType::Rewind5s)
	{
		VK = VK_LEFT;
		ExtendedKey = true;
	}
	else if (Skip == SkipType::Skip5s)
	{
		VK = VK_RIGHT;
		ExtendedKey = true;
	}
	else
		CHECK(false, "Unrecognize skip action");

	SendMessage(Window, WM_KEYDOWN, VK, ExtendedKey ? KF_EXTENDED : 0);
	SendMessage(Window, WM_KEYUP, VK, ExtendedKey ? KF_EXTENDED : 0);
}

void NextVideo(HWND Window)
{
	// Couldnt get this to work with WM_KEYDOWN, so use global keyboard input
	INPUT inputs[4];

	// Shift pressed
	inputs[0].type = INPUT_KEYBOARD;
	inputs[0].ki.wVk = VK_SHIFT;
	inputs[0].ki.dwFlags = KEYEVENTF_EXTENDEDKEY;
	// N pressed
	inputs[1].type = INPUT_KEYBOARD;
	inputs[1].ki.wVk = VK_N;
	// N released
	inputs[2].type = INPUT_KEYBOARD;
	inputs[2].ki.wVk = VK_N;
	inputs[2].ki.dwFlags = KEYEVENTF_KEYUP;
	// Shift released
	inputs[3].type = INPUT_KEYBOARD;
	inputs[3].ki.wVk = VK_SHIFT;
	inputs[3].ki.dwFlags = KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP;

	SendInput(4, inputs, sizeof(INPUT));
}

void PreviousVideo(HWND Window)
{
	SendMessage(Window, WM_KEYDOWN, VK_BACK, 0);
	SendMessage(Window, WM_KEYUP, VK_BACK, 0);
}

void TogglePlay(HWND Window)
{
	SendMessage(Window, WM_KEYDOWN, VK_SPACE, 0);
	SendMessage(Window, WM_KEYUP, VK_SPACE, 0);
}

std::vector<PHYSICAL_MONITOR> Monitors;

BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData) {

	DWORD npm;
	GetNumberOfPhysicalMonitorsFromHMONITOR(hMonitor, &npm);
	auto pPhysicalMonitorArray = std::make_unique<PHYSICAL_MONITOR[]>(npm);

	GetPhysicalMonitorsFromHMONITOR(hMonitor, npm, pPhysicalMonitorArray.get());

	for (unsigned int j = 0; j < npm; ++j) {
		Monitors.push_back(pPhysicalMonitorArray[j]);
	}

	return TRUE;
}

HANDLE GetPrimaryMonitor()
{
	EnumDisplayMonitors(NULL, NULL, MonitorEnumProc, NULL);
	CHECK(Monitors.size() != 0);
	return Monitors[0].hPhysicalMonitor;
}

struct MonitorBrightness
{
	DWORD Minimum, Current, Maximum;
};

MonitorBrightness GetMonitorBrightnessInformation(HANDLE Monitor)
{
	MonitorBrightness MB;
	CHECK(GetMonitorBrightness(Monitor, &MB.Minimum, &MB.Current, &MB.Maximum) == TRUE);
	return MB;
}

void DecreaseBrightness(HANDLE Monitor, float Percentage)
{
	auto MB = GetMonitorBrightnessInformation(Monitor);

	float OneStep = MB.Maximum / 100.0f;
	int Adjustment = Percentage * OneStep;

	int TargetBrightness = MB.Current - Adjustment;
	if (TargetBrightness < static_cast<int>(MB.Minimum))
		TargetBrightness = MB.Minimum;

	CHECK(SetMonitorBrightness(Monitor, static_cast<DWORD>(TargetBrightness)));
}

void IncreaseBrightness(HANDLE Monitor, float Percentage)
{
	auto MB = GetMonitorBrightnessInformation(Monitor);

	float OneStep = MB.Maximum / 100.0f;
	int Adjustment = Percentage * OneStep;

	int TargetBrightness = MB.Current + Adjustment;
	if (TargetBrightness > MB.Maximum)
		TargetBrightness = MB.Maximum;

	CHECK(SetMonitorBrightness(Monitor, static_cast<DWORD>(TargetBrightness)));
}

void HandleMessage(const std::string& Action)
{
	DBG_LOG("Received message %s", Action.c_str());
	auto ActionContains = [&Action](std::string SubStr) {
		return Action.find(SubStr, 0) != std::string::npos;
	};

	if (ActionContains("vol-"))
	{
		VolumeControl VC;
		if (Action == "vol-down")
		{
			VC.DecreaseVolume(10.f, VolumeControl::Unit::Percentage);
		}
		else if (Action == "vol-up")
		{
			VC.IncreaseVolume(10.f, VolumeControl::Unit::Percentage);
		}
	}
	else if (ActionContains("play-") || ActionContains("skip-"))
	{
#if _DEBUG && 0
		Sleep(2000);
#endif
		HWND Window = GetForegroundWindow();
		CHECK(Window != 0);

		char TitleBuffer[256];
		GetWindowText(Window, TitleBuffer, 256);
		std::string Title(TitleBuffer);

		//if (Title.find("YouTube") == std::string::npos) return;

		if (ActionContains("play-"))
		{
			if (Action == "play-toggle")
			{
				TogglePlay(Window);
			}
			else if (Action == "play-next")
			{
				NextVideo(Window);
			}
			else if (Action == "play-prev")
			{
				PreviousVideo(Window);
			}
		}
		else if (ActionContains("skip-"))
		{
			if (Action == "skip-back")
				SkipTime(Window, static_cast<int>(SkipType::Rewind5s));
			else if (Action == "skip-forward")
				SkipTime(Window, static_cast<int>(SkipType::Skip5s));
			else
			{
				size_t DelimiterIndex = Action.find('-');
				CHECK(DelimiterIndex != std::string::npos, "Couldnt find '-' in action string '%s'", Action.c_str());
				std::string SkipTypeStr = Action.substr(DelimiterIndex + 1);
				int Part = std::stoi(SkipTypeStr);
				SkipTime(Window, Part);
			}
		}
	}
	else if (ActionContains("bright-"))
	{
		try
		{
			HANDLE Monitor = GetPrimaryMonitor();
			if (Action == "bright-down")
			{
				DecreaseBrightness(Monitor, 10.f);
			}
			else if (Action == "bright-up")
			{
				IncreaseBrightness(Monitor, 10.f);
			}
		}
		catch (...)
		{
			int Error = GetLastError();
		}
	}
}