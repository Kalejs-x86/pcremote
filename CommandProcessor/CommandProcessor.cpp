#include "Networking/NetworkingTCP.hpp"
#include "MessageHandler.hpp"
#include <iostream>

#pragma comment(lib, "Dxva2.lib")

Common::Network::TCP::CNetworkTCPServer Server;

void OnClientConnected(Common::Network::TCP::clientid ClientId)
{
	DBG_LOG("Client %d connected", ClientId);
	NETMESSAGE Msg;
	auto Client = Server.GetClient(ClientId);
	
	NetResult Result = Server.RecvClient(ClientId, &Msg);
	if (Result == NetResult::Success && Msg.GetOperation() == Common::Network::TCP::Operation::TestString)
	{
		HandleMessage(Msg.ToString());
		Msg.Clear();
		Msg.SetOperation(Common::Network::TCP::Operation::Success);
		Server.SendClient(ClientId, &Msg);
	}
}

int main(int argc, char** argv)
{
	Server.BindPort(8081);
	Server.onClientConnected = OnClientConnected;
	while (true)
	{
		Server.Listen();
	}

	return 0;
}