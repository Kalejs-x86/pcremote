#include "VolumeControl.hpp"
#include "Shared/Exception.hpp"

#include <Functiondiscoverykeys_devpkey.h>
#include <propvarutil.h>
#include <cassert>
#include <limits>

#pragma comment(lib, "Propsys.lib")

VolumeControl::VolumeControl()
{
	CHECK_COM(CoInitialize(NULL));

	IMMDeviceEnumerator* DeviceEnumerator = NULL;
	CHECK_COM(CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&DeviceEnumerator)));

	CHECK_COM(DeviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &DefaultDevice));

	CHECK_COM(DefaultDevice->Activate(__uuidof(IAudioEndpointVolume), CLSCTX_INPROC_SERVER, NULL, reinterpret_cast<void**>(&EndpointVolume)));
	
	IPropertyStore* Props = NULL;
	CHECK_COM(DefaultDevice->OpenPropertyStore(STGM_READ, &Props));

	PROPVARIANT Property;
	PropVariantInit(&Property);
	CHECK_COM(Props->GetValue(PKEY_Device_FriendlyName, &Property));

	wchar_t Buffer[256];
	PropVariantToString(Property, Buffer, 256);
	DefaultDeviceName = std::wstring(Buffer, 256);

	PropVariantClear(&Property);
	Props->Release();
	Props = nullptr;
	DeviceEnumerator->Release();
	DeviceEnumerator = nullptr;
}

VolumeControl::~VolumeControl()
{
	if (DefaultDevice != nullptr)
	{
		DefaultDevice->Release();
		DefaultDevice = nullptr;
	}
	if (EndpointVolume != nullptr)
	{
		EndpointVolume->Release();
		EndpointVolume = nullptr;
	}

	CoUninitialize();
}

constexpr float VolumeControl::GetMinimumValue(Unit Measurement)
{
	switch (Measurement)
	{
	case Unit::Scalar: return 0.f;
	case Unit::Decibel: return -64.f;
	case Unit::Percentage: return 0.f;
	default: assert(false); return std::numeric_limits<float>::max();
	}
}

constexpr float VolumeControl::GetMaximumValue(Unit Measurement)
{
	switch (Measurement)
	{
	case Unit::Scalar: return 1.f;
	case Unit::Decibel: return 0.f;
	case Unit::Percentage: return 100.f;
	default: assert(false); return std::numeric_limits<float>::max();
	}
}

float VolumeControl::GetVolume(Unit Measurement)
{
	float Volume = 0;

	// Decibel adjustment from base level (100% = 0.0dB ; 0% = -64.0db)
	if (Measurement == Unit::Decibel)
	{
		CHECK_COM(EndpointVolume->GetMasterVolumeLevel(&Volume));
	}
	// Scalar value 0.0 - 1.0
	else if (Measurement == Unit::Scalar)
	{
		CHECK_COM(EndpointVolume->GetMasterVolumeLevelScalar(&Volume));
	}
	// Percentage value 0% - 100%
	else if (Measurement == Unit::Percentage)
	{
		CHECK_COM(EndpointVolume->GetMasterVolumeLevelScalar(&Volume));
		Volume *= 100.f;
	}
	else
		assert(false);

	return Volume;
}

void VolumeControl::SetVolume(float Value, Unit Measurement)
{
	if (Measurement == Unit::Decibel)
	{
		CHECK_COM(EndpointVolume->SetMasterVolumeLevel(Value, NULL));
	}
	else if (Measurement == Unit::Scalar)
	{
		CHECK_COM(EndpointVolume->SetMasterVolumeLevelScalar(Value, NULL));
	}
	else if (Measurement == Unit::Percentage)
	{
		Value /= 100.f;
		CHECK_COM(EndpointVolume->SetMasterVolumeLevelScalar(Value, NULL));
	}
	else
		assert(false);
}

void VolumeControl::DecreaseVolume(float Amount, Unit Measurement)
{
	float Volume = GetVolume(Measurement);

	const float MinAmount = GetMinimumValue(Measurement);

	if (Volume - Amount >= MinAmount)
		Volume -= Amount;
	else
		Volume = MinAmount;
	
	SetVolume(Volume, Measurement);
}

void VolumeControl::IncreaseVolume(float Amount, Unit Measurement)
{
	float Volume = GetVolume(Measurement);
	
	const float MaxAmount = GetMaximumValue(Measurement);

	if (Volume + Amount <= MaxAmount)
		Volume += Amount;
	else
		Volume = MaxAmount;

	SetVolume(Volume, Measurement);
}

void VolumeControl::StepDown()
{
	CHECK_COM(EndpointVolume->VolumeStepDown(NULL));
}

void VolumeControl::StepUp()
{
	CHECK_COM(EndpointVolume->VolumeStepUp(NULL));
}
