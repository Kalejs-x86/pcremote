#pragma once
#define NOMINMAX
//#include <windows.h>
#include <mmdeviceapi.h>
#include <endpointvolume.h>
//#include <Audioclient.h>
#include <string>

class VolumeControl
{
public:
	enum class Unit {
		Decibel,
		Scalar,
		Percentage
	};

	VolumeControl();
	~VolumeControl();

	constexpr float GetMinimumValue(Unit Measurement);
	constexpr float GetMaximumValue(Unit Measurement);

	float GetVolume(Unit Measurement);
	void SetVolume(float Value, Unit Measurement);
	void DecreaseVolume(float Amount, Unit Measurement);
	void IncreaseVolume(float Amount, Unit Measurement);
	void StepDown();
	void StepUp();

private:
	std::wstring DefaultDeviceName = L"";
	IMMDevice* DefaultDevice = nullptr;
	IAudioEndpointVolume* EndpointVolume = nullptr;
};