function executeControl(action) {
    $.ajax(
        {
            url: 'control.php',
            type: 'POST',
            data: { action: action} ,
            //contentType: 'application/json; charset=utf-8',
            success: function (response) {
                console.log("Action '" + action + "' completed succesfully");
                console.log("Extra info: " + response);
            },
            error: function () {
                alert("Error during POST request to control.php using action '" + action + "'");
            }
        }
    //).done(function(data) {
    //    console.log(data);
    //}
    );
}

$(document).ready(function(){
    $("a").click(function() {
        executeControl($(this).attr("id"));
    })
    /*
    $("#vol-down").click(function() {
        executeControl("vol-down");
    })
    $("#vol-up").click(function() {
        executeControl("vol-up");
    })
    $("#play-prev").click(function() {
        executeControl("play-prev");
    })
    $("#play-toggle").click(function() {
        executeControl("play-toggle");
    })
    $("#play-next").click(function() {
        executeControl("play-next");
    })*/
 });